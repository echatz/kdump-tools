# kdump-tools po-debconf translation to Spanish.
# Copyright (C) 2021
# This file is distributed under the same license as the kdump-tools package.
# Camaleón <noelamac@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: kdump-tools\n"
"Report-Msgid-Bugs-To: kdump-tools@packages.debian.org\n"
"POT-Creation-Date: 2016-06-10 12:46+0200\n"
"PO-Revision-Date: 2021-04-14 09:16+0200\n"
"Last-Translator: Camaleón <noelamac@gmail.com>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../kdump-tools.templates:1001
msgid "Should kdump-tools be enabled by default?"
msgstr "¿Debería activarse kdump-tools de manera predeterminada?"

#. Type: boolean
#. Description
#: ../kdump-tools.templates:1001
msgid ""
"If you choose this option, the kdump-tools mechanism will be enabled. A "
"reboot is still required in order to enable the crashkernel kernel parameter."
msgstr ""
"Si elije esta opción, se activará el mecanismo kdump-tools. Aún tiene que "
"reiniciar el equipo para activar el parámetro crashkernel del kernel."
